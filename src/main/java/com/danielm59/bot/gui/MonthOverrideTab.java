package com.danielm59.bot.com.danielm59.bot.gui;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class MonthOverrideTab extends JPanel
{
	private final int WIDTH = 750;
	private final int MONTHWIDTH = 100;
	Object[] columnNames = {"Months", "Message"};
	Object[][] data = {};
	JTable overrides;

	public MonthOverrideTab()
	{
		super(true);
		overrides = new JTable(new DefaultTableModel(data,columnNames));
		overrides.setPreferredScrollableViewportSize(new Dimension(WIDTH, 200));
		overrides.getColumnModel().getColumn(0).setPreferredWidth(MONTHWIDTH);
		overrides.getColumnModel().getColumn(1).setPreferredWidth(WIDTH - MONTHWIDTH);
		JScrollPane scrollPanel = new JScrollPane(overrides);
		add(scrollPanel);

		JButton addButton = new JButton("add");
		addButton.setPreferredSize(new Dimension(300, 26));
		addButton.addActionListener(e -> addMessage());
		add(addButton);
	}

	private void addMessage()
	{
		((DefaultTableModel) overrides.getModel()).addRow(new Object[]{null,null});
	}


}
