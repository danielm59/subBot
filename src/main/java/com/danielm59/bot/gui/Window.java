package com.danielm59.bot.com.danielm59.bot.gui;

import com.danielm59.bot.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Window extends Frame
{
	private JTabbedPane tabbedPanel = new JTabbedPane();
	private LogTab logTab = new LogTab();

	public Window() throws HeadlessException
	{
		tabbedPanel.add("Connect", new ConnectTab());
		tabbedPanel.add("Log", logTab);
		tabbedPanel.add("Config", new ConfigTab());
		if (Main.isDebug)
		{
			tabbedPanel.add("Month Overrides", new MonthOverrideTab());
		}

		//init window
		add(tabbedPanel, BorderLayout.CENTER);
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent windowEvent)
			{
				System.exit(0);
			}
		});
		setTitle("Sub Bot");
		setSize(800, 500);
		setVisible(true);
	}

	public void log(String message)
	{
		logTab.guiLog.append(String.format("%s \n\r", message));
	}
}
