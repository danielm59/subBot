package com.danielm59.bot.com.danielm59.bot.gui;

import com.danielm59.bot.Main;
import com.danielm59.bot.Sub;

import javax.swing.*;
import java.awt.*;

public class ConfigTab extends JPanel
{
	private final int WIDTH = 90;

	public ConfigTab()
	{
		super(true);

		JPanel labels = new JPanel(new GridLayout(0, 1));
		JPanel controls = new JPanel(new GridLayout(0, 1));
		JPanel buttons = new JPanel(new FlowLayout(FlowLayout.CENTER));
		add(labels, BorderLayout.WEST);
		add(controls, BorderLayout.EAST);
		add(buttons,BorderLayout.SOUTH);

		Label usernameLabel = new Label("Username: ", Label.RIGHT);
		TextField username = new TextField(Main.getConfig().getUsername(), WIDTH);
		labels.add(usernameLabel);
		controls.add(username);

		Label oAuthLabel = new Label("oAuth: ", Label.RIGHT);
		TextField oAuth = new TextField(Main.getConfig().getoAuth(), WIDTH);
		oAuth.setEchoChar('*');
		labels.add(oAuthLabel);
		controls.add(oAuth);

		Label newSubLabel = new Label("New Sub: ", Label.RIGHT);
		TextField newSub = new TextField(Main.getConfig().getMessages().get(Sub.subType.NEW), WIDTH);
		labels.add(newSubLabel);
		controls.add(newSub);

		Label reSubLabel = new Label("ReSub: ", Label.RIGHT);
		TextField reSub = new TextField(Main.getConfig().getMessages().get(Sub.subType.RESUB), WIDTH);
		labels.add(reSubLabel);
		controls.add(reSub);

		Label primeSubLabel = new Label("New Prime Sub: ", Label.RIGHT);
		TextField primeSub = new TextField(Main.getConfig().getMessages().get(Sub.subType.PRIME), WIDTH);
		labels.add(primeSubLabel);
		controls.add(primeSub);

		JButton save = new JButton("save");
		save.addActionListener(e -> Main.getConfig().saveData(username.getText(),oAuth.getText(),newSub.getText(), reSub.getText(), primeSub.getText()));

		buttons.add(save);
	}
}
