package com.danielm59.bot.com.danielm59.bot.gui;

import javax.swing.*;
import java.awt.*;

public class LogTab extends JPanel
{
	public TextArea guiLog = new TextArea(25, 100);

	public LogTab()
	{
		super(true);
		guiLog.setEditable(false);
		add(guiLog, BorderLayout.CENTER);
	}
}
