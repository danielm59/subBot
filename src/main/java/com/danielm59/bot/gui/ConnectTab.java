package com.danielm59.bot.com.danielm59.bot.gui;

import com.danielm59.bot.M59Bot;
import com.danielm59.bot.Main;
import org.jibble.pircbot.IrcException;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class ConnectTab extends JPanel
{

	private JButton connect;

	public M59Bot bot;

	public ConnectTab()
	{
		super(true);
		JPanel labels = new JPanel(new GridLayout(0, 1));
		JPanel controls = new JPanel(new GridLayout(0, 1));
		JPanel buttons = new JPanel(new FlowLayout(FlowLayout.CENTER));
		add(labels, BorderLayout.WEST);
		add(controls, BorderLayout.EAST);
		add(buttons, BorderLayout.SOUTH);

		Label channelLabel = new Label("Channel: ", Label.RIGHT);
		TextField channel = new TextField(90);
		labels.add(channelLabel);
		controls.add(channel);

		connect = new JButton("connect");
		connect.addActionListener(e -> Connect(channel.getText()));
		buttons.add(connect);

		if (Main.isDebug)
		{
			JButton test = new JButton("test");
			test.addActionListener(e -> System.out.println(bot.getChannels()[0]));
			buttons.add(test);
		}


	}

	private void Connect(String channel)
	{
		bot = new M59Bot(Main.getConfig().getUsername());
		if (Main.isDebug){
			bot.setVerbose(true);
		}
		try
		{
			bot.connect("irc.chat.twitch.tv", 6667, Main.getConfig().getoAuth());
		} catch (IOException e)
		{
			e.printStackTrace();
		} catch (IrcException e)
		{
			e.printStackTrace();
		}
		bot.Connect(channel);
		bot.sendRawLine("CAP REQ :twitch.tv/membership");
		bot.sendRawLine("CAP REQ :twitch.tv/tags");
		bot.sendRawLine("CAP REQ :twitch.tv/commands");
		connect.setEnabled(false);
	}
}
