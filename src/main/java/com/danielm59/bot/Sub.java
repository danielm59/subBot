package com.danielm59.bot;

public class Sub
{
    private subType type;
    private String name;
    private int months = 0;

    Sub(subType type, String name)
    {
        this.type = type;
        this.name = name;
    }

    Sub(subType type, String name, int months)
    {
        this.type = type;
        this.name = name;
        this.months = months;
    }

    subType getType()
    {
        return type;
    }

    String getName()
    {
        return name;
    }

    int getMonths()
    {
        return months;
    }

    String toMessage()
    {
        String message = Main.getConfig().getMessages().get(type);
        message = message.replace("%name%", name);
        message = message.replace("%months%", String.valueOf(months));
        Main.GUI.log(message);
        return message;
    }

    public enum subType
    {
        NEW, PRIME, RESUB
    }
}
