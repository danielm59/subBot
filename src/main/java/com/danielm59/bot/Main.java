package com.danielm59.bot;


import com.danielm59.bot.com.danielm59.bot.gui.Window;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main
{
    //TODO IDEAS
    //Add custom messages for number of months

    public static final boolean isDebug = java.lang.management.ManagementFactory.getRuntimeMXBean().getInputArguments().toString().indexOf("-agentlib:jdwp") > 0;

    static Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private static Config config = new Config();
    static File configFile = new File("config.json");
    public static Window GUI;

    public static void main(String[] args) throws Exception
    {
		loadConfig();
    	GUI = new Window();
    }

    private static void loadConfig()
    {
        if (configFile.exists() && !configFile.isDirectory())
        {
            try (FileReader reader = new FileReader(configFile))
            {
                JsonReader jsonReader = new JsonReader(reader);
                config = gson.fromJson(jsonReader, Config.class);
            } catch (IOException e)
            {
                e.printStackTrace();
            }

        } else
        {
            try (FileWriter writer = new FileWriter(configFile))
            {
                gson.toJson(config, writer);
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    public static Config getConfig()
    {
        return config;
    }
}
