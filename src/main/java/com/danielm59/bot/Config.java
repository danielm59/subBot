package com.danielm59.bot;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class Config
{
    private String username;
    private String oAuth;
    private HashMap<Sub.subType, String> messages = new HashMap<>();

    Config()
    {
        username = "username";
        oAuth = "oauth:";
        messages.put(Sub.subType.NEW, "Thank you %name% for the sub");
        messages.put(Sub.subType.RESUB, "Thank you %name% for the %months% months resub");
        messages.put(Sub.subType.PRIME, "Thank you %name% for the sub with twitch prime");
    }

    public String getUsername()
    {
        return username;
    }

    public String getoAuth()
    {
        return oAuth;
    }

    public HashMap<Sub.subType, String> getMessages()
    {
        return messages;
    }

    public void saveData(String username, String oAuth, String newSub, String reSub, String primeSub)
    {
        this.username = username;
        this.oAuth = oAuth;
        messages.put(Sub.subType.NEW, newSub);
        messages.put(Sub.subType.RESUB, reSub);
        messages.put(Sub.subType.PRIME, primeSub);

        try (FileWriter writer = new FileWriter(Main.configFile))
        {
            Main.gson.toJson(this, writer);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}