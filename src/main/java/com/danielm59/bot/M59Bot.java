package com.danielm59.bot;

import org.jibble.pircbot.PircBot;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class M59Bot extends PircBot
{
    private Pattern loginRegex = Pattern.compile(".*login=(\\w*);.*");

    private HashMap<Sub.subType, Pattern> subRegex = new HashMap<>();

    private String channel;

    public M59Bot(String username)
    {
        this.setName(username);
        subRegex.put(Sub.subType.NEW, Pattern.compile(".*:(\\w*) just subscribed!"));
        subRegex.put(Sub.subType.PRIME, Pattern.compile(".*:(\\w*) just subscribed with Twitch Prime!"));
        subRegex.put(Sub.subType.RESUB, Pattern.compile(".*display-name=(\\w*).*msg-id=resub;msg-param-months=(\\d*).*"));
    }

    @Override
    protected void onConnect()
    {
        Main.GUI.log("Connected to Twitch chat server");
    }

    @Override
    protected void onDisconnect()
    {
        Main.GUI.log("Disconnected");
        try
        {
            Main.GUI.log("Attempting to reconnect");
            reconnect();
        } catch (Exception e)
        {
            Main.GUI.log("Reconnect failed, waiting 10 seconds before retrying");
            try
            {
                Thread.sleep(10000);
            } catch (InterruptedException e2)
            {
                Thread.currentThread().interrupt();
            }
        }
    }


    @Override
    public void log(String line)
    {
        super.log(line);
        if (line.contains("twitchnotify") || line.contains("USERNOTICE"))
        {
            try
            {
                String filename = "Log.txt";
                FileWriter fw = new FileWriter(filename, true); //the true will append the new data
                fw.write(line + "\r\n");
                Sub sub = testString(line);
                if (sub != null)
                {
                    fw.write(sub.toMessage() + "\r\n");//appends the string to the file
                    sendMessage(channel, sub.toMessage());
                }
                fw.close();
            } catch (IOException ioe)
            {
                System.err.println("IOException: " + ioe.getMessage());
            }
        }
    }

    private Sub testString(String line)
    {
        for (Map.Entry<Sub.subType, Pattern> typeRegex : subRegex.entrySet())
        {
            Sub.subType type = typeRegex.getKey();
            Pattern regex = typeRegex.getValue();
            Matcher m = regex.matcher(line);
            if (m.matches())
            {
                String name = m.group(1);
                if (name.isEmpty())
                {
                    name = fixName(line);
                }
                if (type == Sub.subType.RESUB)
                {
                    try
                    {
                        int months = Integer.parseInt(m.group(2));
                        return new Sub(type, name, months);
                    } catch (NumberFormatException e)
                    {
                        return new Sub(type, name);
                    }
                }
                return new Sub(type, name);
            }
        }
        return null;
    }

    private String fixName(String line)
    {
        Matcher name = loginRegex.matcher(line);
        if (name.matches())
        {
            return name.group(1).substring(0, 1).toUpperCase() + name.group(1).substring(1);
        } else
        {
            return "UNKNOWN";
        }
    }

    public void Connect(String channel)
    {
        this.channel = "#" + channel;
        joinChannel(this.channel);
    }
}
